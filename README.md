## Dark Crystal Dapp

Built with Typescript, NextJS, React and TailwindCSS

### Prototype Dapp for use by recovery-partners

Proof-of-concept deployed at: https://web3.darkcrystal.pw/

### Requirements:

Metamask (or eventually any browser-based wallet) installed

##### To create a share:

The dapp will request to connect with an existing browser-based web3 wallet (currently only Metamask). The recovery-partner will enter a phrase (by default this will be the ethereum address of the secret-owner, though the secret-owner can choose to override this) which they will then sign with their ethereum private key. From the resulting hash, the dapp will deterministically derive a keypair. The public key will be displayed, which the recovery-partner can send to the secret-owner. The private key will be discarded. The secret-owner will then use the public key, along with others collected from their chosen number of recovery-partners, to 'share out' their secret and store the resulting pieces on the blockchain.

##### To recover a share: 

To recover a share, the above process will be repeated. The dapp will then make a call to the Secrets smart contract where the shares are stored, to recover the share associated with their lookup key. The dapp will then decrypt the share, which the shareholder can send to the Secret-Owner. When enough shareholders have decrypted and sent their shares to a Secret-Owner, the Secret-Owner can combine these to recover their secret.

### For Developers

#### Requirements

- Node JS
- Metamask (or eventually any other wallet) installed

optional (see contracts repo for set-up instructions):
- dark-crystal-contracts repo cloned
- truffle suite/ganache installed
### Usage

##### Fetch the latest version

```sh
git pull
npm install
```
##### Run tests

```sh
npm run test
```
##### Start local server

```sh
npm run dev
```

open `localhost:3000` in a browser

### License

GNU Affero General Public License v3.0 `AGPL-3.0`