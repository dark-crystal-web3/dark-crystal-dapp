import React from 'react';
import Link from 'next/link';

const Footer = () => {
  return (
    <footer className="relative inset-x-0 bottom-0 flex flex-row items-center w-full mx-auto bg-barpink
    h-[40px] sm:h-[56px]
    ">
      <div className="w-full font-silkscreen font-normal text-center text-very-light-grey
      h-[15px] sm:h-[27px]
      text-[12px] sm:text-lg
      leading-[150%]
      ">
        <Link href="https://darkcrystal.pw">
          <a>darkcrystal.pw</a>
        </Link>
        <span> | </span>
        <Link href="https://magmacollective.org">
          <a>magmacollective.org</a>
        </Link>
      </div>
    </footer>
  )
}

export default Footer;
