import React from 'react'
import { PageHeaderText } from 'types/types'
import Navbar from './Navbar'
import IconBox from './IconBox'

type Props = {
  Heading: PageHeaderText
}

const Header = ({ Heading }: Props) => {
  return (
    <div className="flex-col items-center self-center p-0 w-full h-full object-cover top-0 left-0 bg-center align-baseline bg-hero-pattern">
      <Navbar />
      <div className="px-10 lg:pl-44 mb-12">
        <div className="flex flex-row gap-3 align-center items-center">
          <IconBox option={Heading} />
          <div className="font-['Silkscreen'] font-normal text-4xl lg:text-6xl tracking-tighter text-white p-4 align-center">{Heading.title}</div>
        </div>
        <div className="font-['Inter'] font-extrabold text-base text-very-light-grey">{Heading.subtitle}</div>
        <div className="hidden sm:block font-['Inter'] text-sm text-very-light-grey">{Heading.subsubtitle}</div>
      </div>
    </div>
  )
}

export default Header;
