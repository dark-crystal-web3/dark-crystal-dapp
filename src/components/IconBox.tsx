import React from 'react';
import { MainMenuOption, PageHeaderText } from '../types/types'

type Props = {
  option: MainMenuOption | PageHeaderText
}

const IconBox = ({ option }: Props) => {

  const icon = `/img/${option.icon}.png`

  return (
    <div className="flex grow-0 rounded-md p-2 justify-center items-center bg-very-dark-pink group-hover:bg-strong-pink
    w-[45px] sm:w-[60px] xl:w-[68px] xxl:w-[74px]
    h-[45px] sm:h-[60px] xl:w-[68px] xxl:h-[74px] 
    ">
      <img
        src={icon}
        alt={option.alt}
      />
    </div>
  )
}

export default IconBox;