import React, { useState } from 'react';
import Link from 'next/link';
import IconBox from './IconBox'
import { MainMenuBackup, MainMenuRecover, MainMenuGenerate, MainMenuDecrypt, MainMenuOwner, MainMenuPartner, FAQ, About } from '../services/copy';

const Menu = () => {

  const [dropdown, setDropdown] = useState<Boolean>(false)

  const owner = { user: MainMenuOwner, options: [MainMenuBackup, MainMenuRecover] }
  const partner = { user: MainMenuPartner, options: [MainMenuGenerate, MainMenuDecrypt] }
  const info = [FAQ, About]


  function hamburgerClick() {
    setDropdown(!dropdown)
  }

  function screenClick() {
    setDropdown(false)
  }

  // TODO add in tab-navigation, enter-select & esc-close

  return (
    <div>
      <button onClick={hamburgerClick}>
        <img
          src={'/img/hamburger.png'}
          alt="Hamurger Icon"
        />
      </button>
      {!!dropdown ?
        <div>
          <button onClick={screenClick} className="z-10 fixed inset-0 h-full w-full bg-black opacity-40 cursor-default"></button>
          <div className="absolute z-20 right-0 top-1">
            {[owner, partner].map(item =>
              <div key={`${item.user.colour}`} className={`flex flex-col box-content border-x-8 border-y-4 border-very-dark-pink ${item.user.colour} 
              p-4 sm:p-8
              gap-[20px] sm:gap-[30px]
              w-[210px] sm:w-[300px]
              h-[190px] sm:h-[210px]`}>
                <div className="flex flex-col gap-2 sm:gap-6">
                  <div key={`${item.user.menu}`} className="leading-8 font-silkscreen font-normal text-very-dark-pink opacity-90
                  sm:text-[24px]
                  " >{item.user.menu}
                  </div>
                  {item.options.map(option =>
                  <div key={`${option.title}`} className="hover:bg-white cursor-pointer">
                    <Link href={option.page}>
                      <div className="flex flex-row items-center gap-4">
                        <div>
                          <IconBox
                            option={option}
                          />
                          </div>
                          <div>
                            <a className="leading-8 font-silkscreen font-normal text-very-dark-pink opacity-90 sm:text-[24px]">{option.title}</a>
                          </div>
                      </div>
                    </Link>
                    </div>
                  )}
                </div>
              </div>
            )}
            <div className="flex flex-col box-content border-x-8 border-t-4 border-b-8 border-very-dark-pink bg-barpink 
            p-4 sm:p-8
            gap-[20px] sm:gap-[30px]
            w-[210px] sm:w-[300px]
            h-[70px] sm:h-[90px]
            ">
              {info.map(item =>
                <Link href={item.page} key={`${item.title}`}>
                  <div className="font-silkscreen font-normal text-off-white order-0 grow-0 hover:bg-white hover:text-very-dark-pink cursor-pointer
                  h-[30px] 
                  text:xs sm:text-[20px]
                  leading-[150%]
                  ">{item.title}</div>
                </Link>
              )}
            </div>
          </div>
        </div> :
        <div />
      }
    </div >
  )
}

export default Menu;