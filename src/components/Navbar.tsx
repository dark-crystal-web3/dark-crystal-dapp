import React from 'react';
import Link from 'next/link';
import Menu from './Menu'
import { AppName, AppTag } from '../services/copy';


const Navbar = () => {
  return (
    <nav>
      <div className="flex flex-row self-center justify-between bg-gradient-to-b from-barpink to-bar-pink-end
      w-full
      h-[80px] sm:h-[120px] 
      top-0
      gap-2 sm:gap-8
      ">

        {/* LEFT SIDE */}
        <div className="flex flex-row self-center 
        w-full 
        gap-2 
        ml-4 sm:ml-10
        ">
          {/* CRYSTALS ICON */}
          <div className="flex shrink-0 justify-center bg-very-dark-pink shadow-strong-pink shadow-8px p-2
          w-[38px] sm:w-[64px]
          h-[38px] sm:h-[64px]
          " >
            <img
              src={'/img/crystalsRecovered.png'}
              alt="Bouquet of crystals" />
          </div>

          {/* APP NAME */}
          <div className="flex flex-col self-center
          shrink sm:shrink-0
          ">
            {/* DARK CRYSTAL */}
            <div>
              <Link href="/">
                <a className="font-silkscreen text-very-light-grey mr-0 p-0
                w-full
                h-[15px] sm:h-[34px]
                text-base sm:text-3xl
                ">{AppName}</a>
              </Link>
            </div>
            {/* SOCIAL RECOVERY DAPP */}
            <div>
              <Link href="/">
                <a className="font-inter font-normal leading-over text-left text-very-light-grey
                w-full
                sm:h-[20px]
                text-xs">{AppTag}</a>
              </Link>
            </div>
          </div>
        </div>

        {/* RIGHT SIDE */}
        {/* HAMBURGER */}
        <div className="flex shrink-0
        self-start
        mt-6 sm:mt-10
        mr-4 sm:mr-10 
        ml-0 
        p-0">
          <Menu />
        </div>

      </div>
    </nav>
  );
}

export default Navbar;
