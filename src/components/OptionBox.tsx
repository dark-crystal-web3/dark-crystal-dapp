import React from 'react';
import Link from 'next/link';
import IconBox from './IconBox'
import { MainMenuHeader, MainMenuOption } from '../types/types'

type Props = {
  user: MainMenuHeader,
  options: [MainMenuOption, MainMenuOption]
}

const BoxMain = ({ user, options }: Props) => {

  return (
    <div className="flex flex-col
    p-8 xl:p-10
    gap-5 xl:gap-10
    ">

      {/* USER */}
      <div className="flex flex-col left-[0px]
      gap-[6px] sm:gap-[15px]
      ">
        <div className="font-silkscreen font-normal leading-10 tracking-tighter text-very-dark-pink
        text-[22px] sm:text-3xl xl:text-[30px]
        ">{user.title}</div>
        <div className="font-inter font-normal leading-2 text-barpink 
        text-[14px] sm:text-[16px]
        ">{user.subtitle}</div>
      </div>

      {/* OPTION */}
      <div>
        <div className="flex
        flex-col xl:flex-row
        max-[1280px]:h-[50%]
        gap-4
        ">
          {options.map(option =>
            <div key={option.title} className="flex flex-col xl:min-w-[50%] p-2 gap-4
            ">
              {/* HEADING */}
              <div className="flex flex-row gap-3 items-center group
              h-[50px] sm:h-[65px] xl:h-[80px] 
              ">
                <div>
                  <IconBox
                    option={option}
                  />
                </div>
                <div>
                  <Link href={option.page}>
                    <a className="items-center font-silkscreen font-normal text-very-dark-pink group-hover:text-strong-pink opacity-90 
                    text-[20px] sm:text-2xl xl:text-xl xxl:text-2xl 
                    leading-6 sm:leading-8
                    ">{option.title}</a>
                  </Link>
                </div>
              </div>

              {/* BODY */}
              <div className="flex flex-col basis-auto 
              gap-2 sm:gap-4">
                <div className="flex flex-none self-stretch grow-0 font-inter font-semibold text-very-dark-pink leading-6
                text-[14px] sm:text-[16px]
                ">{option.subtitle}</div>
                <div className="flex flex-none order-1 self-stretch grow-0 font-inter font-normal leading-6 text-very-dark-pink opacity-80
                text-[12px] sm:text-[14px]
                ">{option.text}</div>
              </div>
            </div>
          )}
        </div>
      </div>

      {/* LINK TO LOCAL DAPP */}
      {user.title === "I HAVE A SECRET" ?
        <Link href={'https://darkcrystal.pw/web3'}>
          < a className="flex font-inter font-normal text-very-dark-pink hover:underline text-start leading-2
        pb-4 pt-2 pl-1
        text-[14px] sm:text-[16px]
        ">{'->  Learn how to run your own local instance of the Dapp'}
          </a>
        </Link> :
        <div />
      }
    </div >
  );
};

export default BoxMain;
