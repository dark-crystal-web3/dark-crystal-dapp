import React from 'react'
import Footer from './Footer';
import Header from './Header';
import { PageHeaderText } from '../types/types'

type Props = {
  heading: PageHeaderText,
  bgColour: string,
  children: React.ReactNode,
}

const PageContainer = ({ heading, bgColour, children }: Props) => {

  return (
    <div className="bg-very-pale-pink">
      <div className={`flex flex-col min-h-screen ${bgColour} bg-cover"`}>
        <Header Heading={heading} />
        <div className={`flex flex-col min-h-screen p-5 sm:px-10 lg:pl-44 max-w-6xl ${bgColour}`}>
          {children}
        </div>
        <Footer />
      </div>
    </div>
  )
}

export default PageContainer;
