import React from 'react'
import { Threshold, TextInputHeader, Button, text, icons } from 'dark-crystal-web3-ui-components';

// TODO reset / cancel button

type Props = {
  n: number,
  m: number,
  shareLink: string,
}

const ShareSuccess = ({ n, m, shareLink }: Props) => {
  const headerText = { label: text.backupCreatedTitle };

  const qs = new URL(shareLink).search;

  return (
    <div>
      <TextInputHeader number="1" text={ headerText } />
      <Threshold n={ n } m={ m } />
      <TextInputHeader number="2" text={ text.publishAction } />
      <div className="flex justify-between mt-4">
        <div />
        <div>
          { /* TODO in production this will be <a href={ shareLink }> */ }
          <a href={ `/store${qs}` }>
            <Button text={ text.publishAndStoreButton } icon={ icons('cofre-abierto') } />
          </a>
        </div>
      </div>
    </div>
  )
}

export default ShareSuccess;
