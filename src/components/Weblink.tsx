import React from 'react';
import styled from 'styled-components';

type Props = {
  link: string,
  text: string
}

const WebLink = ({ link, text } : Props) => {

  return (
    <WebLinkStyle>
      <a href={link} rel="noreferrer" style={{ textDecoration: 'none', color: 'white' }}>{text}</a>
    </WebLinkStyle >
  );
};

export const WebLinkStyle = styled.div`
color: white;
font-size: 1rem;
padding: 1rem;
`;

export default WebLink;
