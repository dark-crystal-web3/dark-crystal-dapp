import PageContainer from '../components/PageContainer'
import { PageHeader404 } from '../services/copy'
import { useRouter } from 'next/router'

export default function Custom404() {
  const router = useRouter()
  PageHeader404.subtitle = `Cannot find ${router.asPath}`
  return (
    <PageContainer heading={ PageHeader404 } bgColour={'bg-secret-owner'}>
    </PageContainer>
  )
}
