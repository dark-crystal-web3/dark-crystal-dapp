import PageContainer from '../components/PageContainer';
import ShareSuccess from '../components/ShareSuccess';
import { Share, text } from 'dark-crystal-web3-ui-components';
import * as dc from '../services/share-recover';
import React, { useState, useEffect } from 'react';
import Connect from '../services/connectWeb3'
import { Owner } from '../types/types';

// Setting this to false removes the 'create test data' button
const testMode = true;

const Backup = () => {
  const [owner, setOwner] = useState<Owner>({})

  const fetchOwner = async (): Promise<Owner> => {
    const result = await Connect();
    console.log('setting owner')
    setOwner(result)
    return result
  }

  useEffect(() => {
    fetchOwner()
  }, [])

  // Detect change in Metamask account
  useEffect(() => {
    if (window.ethereum) {
      window.ethereum.on("chainChanged", () => {
        fetchOwner();
      });
      window.ethereum.on("accountsChanged", () => {
        fetchOwner();
      });
    }
  });

  const heading = {
    icon: 'cofre-cerrado',
    alt: 'Closed chest icon',
    title: text.pageHeadings.backup.text,
    subtitle: text.pageHeadings.backup.subtext,
    subsubtitle: text.pageHeadings.backup.subsubtext,
  }

  const ethAddress = owner.accounts ? owner.accounts[0] : undefined
  console.log(ethAddress)
  return (
    <PageContainer heading={ heading } bgColour={'bg-secret-owner'}>
      <Share visible="true" dc={ dc } testMode={ testMode } ethAddress={ ethAddress } ShareSuccess={ ShareSuccess } />
    </PageContainer>
  );
}

export default Backup;
