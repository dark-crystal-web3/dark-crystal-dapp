import React, { useState, useEffect } from 'react';
import assert from 'assert';
import PageContainer from '../components/PageContainer';
import Connect from '../services/connectWeb3'
import createKeyPair from '../services/createKeys';
import { Owner, Keypair } from '../types/types'
import {
  PageHeaderDecrypt,
  PageHeaderDecryptSuccess,
  PageContentDecryptOne,
  PageContentDecryptTwo,
  PageContentDecryptSuccess,
  PageContentDecryptError
} from '../services/copy'
import {
  TextInputHeader,
  TextInput,
  TextBox,
  Button
} from 'dark-crystal-web3-ui-components'


const Sodium = require('sodium-javascript');

function Decrypt() {
  const [owner, setOwner] = useState<Owner>({})
  const [share, setShare] = useState<string>('')
  const [text, setText] = useState<string>('')
  const [lookup, setLookup] = useState<string>('')
  const [error, setError] = useState<string>('')

  const fetchOwner = async (): Promise<Owner> => {
    const result = await Connect();
    setOwner(result)
    return result
  }

  useEffect(() => {
    fetchOwner()
  }, [])

  // Detect change in Metamask account
  useEffect(() => {
    if (window.ethereum) {
      window.ethereum.on("chainChanged", () => {
        fetchOwner();
      });
      window.ethereum.on("accountsChanged", () => {
        fetchOwner();
      });
    }
  });

  const retrieveShare = async (lookupKey: string) => {
    if (owner.contract) {
      // returns two buffers, newpublickey & newsecretkey
      const result: Keypair = await createKeyPair(owner)
      if (result.newPublicKey && result.newSecretKey) {
        assert(result.newPublicKey.length >= Sodium.crypto_generichash_KEYBYTES_MIN && result.newPublicKey.length <= Sodium.crypto_generichash_KEYBYTES_MAX)
        assert(result.newSecretKey.length >= Sodium.crypto_generichash_KEYBYTES_MIN && result.newSecretKey.length <= Sodium.crypto_generichash_KEYBYTES_MAX)

        const storeKeyBuf: Buffer = Buffer.alloc(Sodium.crypto_generichash_BYTES)
        const lookupKeyBuf: Buffer = Buffer.from(lookupKey)
        Sodium.crypto_generichash(storeKeyBuf, lookupKeyBuf, result.newPublicKey)

        const storageKey: string = "0x".concat(storeKeyBuf.toString('hex'))

        if (owner.contract) {
          const contract = owner.contract;
          contract.methods.getShare(storageKey).call()
            .then((Result: any) => {
              // Result.text is the nonce + ciphertext
              const textBuf: Buffer = Buffer.from(Result.text.slice(2), 'hex')
              const nonceBuf: Buffer = textBuf.subarray(0, 24)
              setText(textBuf.toString('hex'))
              // result.share is the recovery partner's encrypted share
              const shareBuf: Buffer = Buffer.from(Result.share.slice(2), 'hex')
              // this is the secret-owner's ephemeral public key, required for decrypting each share
              const ephPKBuf: Buffer = Buffer.from(Result.epubkey.slice(2), 'hex')
              if (result.newSecretKey) {
                assert(result.newSecretKey.length >= Sodium.crypto_generichash_KEYBYTES_MIN && result.newSecretKey.length <= Sodium.crypto_generichash_KEYBYTES_MAX)
                // attempt to decrypt the recovery partner's encrypted share
                return decrypt(shareBuf, nonceBuf, ephPKBuf, result.newSecretKey)
              }
            })
            .catch(() => {
              setError("No share found")
            })
        }
      }
    } else {
      console.log('please connect your wallet and try again')
    }
  };

  const decrypt = (cipherShareBuf: Buffer, nonce: Buffer, ephPubKey: Buffer, sKey: Buffer) => {
    const plaintextBuf: Buffer = Buffer.alloc(cipherShareBuf.length - Sodium.crypto_box_MACBYTES)
    assert(cipherShareBuf.length > Sodium.crypto_box_MACBYTES, 'share too short')
    const decrypted = Sodium.crypto_box_open_easy(plaintextBuf, cipherShareBuf, nonce, ephPubKey, sKey)
    if (!decrypted) throw new Error('Decryption failed')
    setShare(plaintextBuf.toString('hex'))
  };

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLookup(e.target.value)
  }

  const handleOnClick = () => {
    retrieveShare(lookup)
  }

  const handleTryAgain = () => {
    setError('')
    setLookup('')
    setText('')
  }

  return (
    <div>
      {!share.length ?
        (<PageContainer heading={PageHeaderDecrypt} bgColour={'bg-off-white'}>
          {!error.length ?
            (
              <div>
                <TextInputHeader
                  number={'1'}
                  text={PageContentDecryptOne}
                />
                <TextBox
                  icon={<img src={'/img/ethIcon.png'} alt="Ethereum Icon" />}
                  content={owner.accounts ? owner.accounts[0] : ''}
                  hideCopyButton={true}
                />
                <TextInputHeader
                  number={'2'}
                  text={PageContentDecryptTwo}
                />
                <TextInput
                  icon={<img src={'/img/ethIcon.png'} alt="Ethereum Icon" />}
                  placeholder={PageContentDecryptTwo.placeholder}
                  value={lookup}
                  onChange={handleInput}
                  readOnly={false}
                />
                <div className="mt-4 flex justify-end">
                  <Button
                    onClick={handleOnClick}
                    disabled={!owner}
                    text={'Decrypt'}
                    icon={<img src={'/img/keyDecrypt.png'} alt="key decrypt Icon" />}
                  />
                </div>
              </div>
            ) : (
              <div>
                <TextInputHeader
                  number={'X'}
                  text={PageContentDecryptError}
                />
                <div className="mt-4 flex justify-end">
                  <Button
                    onClick={handleTryAgain}
                    disabled={!owner}
                    text={'Try again'}
                    icon={<img src={'/img/keyDecrypt.png'} alt="key decrypt Icon" />}
                  />
                </div>
              </div>

            )
          }
        </PageContainer >
        ) : (
          <PageContainer heading={PageHeaderDecryptSuccess} bgColour={'bg-off-white'}>
            <TextInputHeader
              number={'1'}
              text={PageContentDecryptSuccess}
            />
            <div className="break-all">
              <TextBox
                icon={<img src={'/img/keyDecrypt.png'} alt="key decrypt Icon" />}
                text={`${share}${text}`}
              />
            </div>

          </PageContainer>
        )
      }
    </div>
  )
}

export default Decrypt;
