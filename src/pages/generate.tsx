import React, { useState, useEffect } from 'react';
import { TextInputHeader, TextInput, TextBox, Button } from 'dark-crystal-web3-ui-components'
import PageContainer from '../components/PageContainer';
import createKeyPair from '../services/createKeys';
import Connect from '../services/connectWeb3'
import {
  PageHeaderGenerate,
  PageHeaderGenerateSuccess,
  PageContentGenerate,
  PageContentGenerateSuccess
} from '../services/copy'
import { Owner, Key, Keypair } from '../types/types';


function Generate() {

  const [owner, setOwner] = useState<Owner>({})
  const [pubKey, setPubKey] = useState<Key>('')

  const fetchOwner = async (): Promise<Owner> => {
    const result = await Connect();
    setOwner(result)
    return result
  }

  useEffect(() => {
    fetchOwner()
  }, [])

  // Detect change in Metamask account
  useEffect(() => {
    if (window.ethereum) {
      window.ethereum.on("chainChanged", () => {
        fetchOwner();
      });
      window.ethereum.on("accountsChanged", () => {
        fetchOwner();
      });
    }
  });

  const generateKey = async (owner: Owner) => {
    const result: Keypair = await createKeyPair(owner)
    if (result.newPublicKey) {
      setPubKey(result.newPublicKey.toString('hex'))
    }
  };

  return (
    <div>
      {!pubKey.length ?
        (
          <PageContainer heading={PageHeaderGenerate} bgColour={'bg-off-white'}>
            <TextInputHeader
              number={'1'}
              text={PageContentGenerate}
            />
            <TextBox
              icon={<img src={'/img/ethIcon.png'} alt="Ethereum Icon" />}
              content={owner.accounts ? owner.accounts[0] : ''}
              hideCopyButton={true}
            />
            <div className="mt-4 flex justify-end">
              <Button
                onClick={generateKey}
                disabled={!owner}
                text={'Generate'}
                icon={<img src={'/img/keyGenerate.png'} alt="key generate Icon" />}
              />
            </div>
          </PageContainer>
        ) : (
          <PageContainer heading={PageHeaderGenerateSuccess} bgColour={'bg-off-white'}>
            <TextInputHeader
              number={'1'}
              text={PageContentGenerateSuccess}
            />
            <TextBox
              icon={<img src={'/img/keyGenerate.png'} alt="key generate Icon" />}
              text={pubKey}
            />
          </PageContainer>
        )
      }
    </div>
  )
}

export default Generate;
