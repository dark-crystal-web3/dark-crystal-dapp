import 'react-tabs/style/react-tabs.css';
import React from 'react';
import Link from 'next/link';
import OptionBox from '../components/OptionBox';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { MainMenuBackup, MainMenuRecover, MainMenuGenerate, MainMenuDecrypt, MainMenuOwner, MainMenuPartner } from '../services/copy';

function App() {
  return (
    <div className="bg-barpink bg-cover">
      <div className="flex flex-col max-w-screen-2xl min-h-screen mx-auto">
        <div className="flex self-center
        w-full
        h-[400px] sm:h-[500px] xl:h-full
        ">
          <img
            src={'/img/backgroundMain.png'}
            alt="Hero image is a dark cave painted in purple hues, with coloured crystals growing in clumps and floating suspended throughout the piece. A waterfall from the left, a lake on the right and stairs leading outside at the back"
            className="object-fill w-full"
          />
        </div>
        <div className="max-w-screen-2xl w-full absolute top-0 self-center items-center">
          <Navbar />
        </div>
        <div className="w-full">

          {/* BACKUP YOUR SECRETS */}
          <div className="absolute translate-x-[-50%] left-[50%] bg-off-white shadow-barpink shadow-8px font-silkscreen font-normal text-strong-pink text-center align-center align-middle tracking-tighter
          w-[290px] sm:w-[400px] md:w-[584px]
          h-[30px] sm:h-[40px] md:h-[47px]
          top-[130px] sm:top-[170px] xl:top-[222px]
          text-[22px] sm:text-3xl md:text-[45px]
          leading-[120%] md:leading-[100%]
          ">Backup your secrets</div>

          {/* USING THE TRUST IN YOUR*/}
          <div className="absolute translate-x-[-50%] left-[50%] bg-very-dark-pink shadow-secret-owner shadow-6px font-silkscreen font-normal text-secret-owner text-center align-center align-middle tracking-tighter
          w-[240px] sm:w-[300px] md:w-[454px]
          h-[30px] md:h-[42px]
          top-[180px] sm:top-[230px] xl:top-[290px]
          text-[15px] sm:text-[20px] md:text-[30px]
          leading-[200%] sm:leading-[150%] md:leading-[120%]
          ">Using the trust in your</div>
          {/* SOCIAL FABRIC */}
          <div className="absolute translate-x-[-50%] left-[50%] bg-very-dark-pink shadow-secret-owner shadow-6px font-silkscreen font-normal text-secret-owner text-center align-center tracking-tighter
          w-[140px] sm:w-[180px] md:w-[280px]
          h-[30px] md:h-[42px]
          top-[202px] sm:top-[255px] md:top-[265px] xl:top-[320px]
          text-[15px] sm:text-[20px] md:text-[30px]
          leading-[200%] sm:leading-[150%] md:leading-[120%]
          ">Social fabric</div>

          {/* SECURE MANAGEMENT OF SENSITIVE DATA ON THE DISTRIBUTED WEB */}
          <div className="absolute translate-x-[-50%] left-[50%] font-inter font-normal text-very-light-grey text-center align-center
          w-[220px] sm:w-[270px] md:w-[395px]
          md:h-[48px]
          top-[250px] sm:top-[310px] md:top-[320px] xl:top-[385px]
          text-[12px] sm:text-[14px] md:text-[16px]
          sm:leading-[150%]
          ">Secure management of sensitive data on the distributed web <Link href={'https://darkcrystal.pw/web3'}><a className="hover:underline">{"-> Know more"}</a></Link>
          </div>
        </div>


        <div className="flex relative
        -top-[60px] sm:-top-[90px] xl:-top-[130px]
        ">
          {/* TODO: figure out why min & max size attr. don't work => max-md:items-center */}
          <div className="flex gap-10 w-full items-center md:items-start md:justify-center md:items-stretch
          flex-col md:flex-row
          ">
            <div className="flex grow-1 bg-secret-owner shadow-8px shadow-very-dark-pink
            w-[90%] md:w-[45%]
            ">
              <OptionBox
                user={MainMenuOwner}
                options={[MainMenuBackup, MainMenuRecover]}
              />
            </div>
            <div className="flex grow-1 bg-off-white shadow-8px shadow-very-dark-pink
            w-[90%] md:w-[45%]
            ">
              <OptionBox
                user={MainMenuPartner}
                options={[MainMenuGenerate, MainMenuDecrypt]}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="flex relative -top-[40px] sm:-top-[56px]">
        <Footer />
      </div>
    </div >
  )
};

export default App;
