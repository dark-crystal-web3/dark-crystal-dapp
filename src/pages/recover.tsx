import PageContainer from '../components/PageContainer';
import { Recover as RecoverComponent, text } from 'dark-crystal-web3-ui-components';
import * as dc from '../services/share-recover';
import React, { useState, useEffect } from 'react';
import Connect from '../services/connectWeb3'
import { Owner } from '../types/types';

// Setting this to false removes the 'create test data' button
const testMode = true;

const Recover = () => {
  // TODO get eth address

  const [owner, setOwner] = useState<Owner>({})

  const fetchOwner = async (): Promise<Owner> => {
    const result = await Connect();
    setOwner(result)
    return result
  }

  useEffect(() => {
    fetchOwner()
  }, [])

  // Detect change in Metamask account
  useEffect(() => {
    if (window.ethereum) {
      window.ethereum.on("chainChanged", () => {
        fetchOwner();
      });
      window.ethereum.on("accountsChanged", () => {
        fetchOwner();
      });
    }
  });


  const ethAddress = owner.accounts ? owner.accounts[0] : undefined

  type ContainerProps = {
    children: React.ReactNode,
    success: boolean,
  }

  const RecoverContainer = ({ children, success }: ContainerProps) => {
    const heading = success
      ? {
          icon: 'successCup',
          alt: 'Trophy icon',
          title: text.pageHeadings.recoverSuccess.text,
          subtitle: text.pageHeadings.recoverSuccess.subtext,
        }
      : {
          icon: 'cofre-abierto',
          alt: 'Open chest icon',
          title: text.pageHeadings.recover.text,
          subtitle: text.pageHeadings.recover.subtext,
          subsubtitle: text.pageHeadings.recover.subsubtext,
        }

    return (
      <PageContainer heading={ heading } bgColour={'bg-secret-owner'}>
        { children }
      </PageContainer>
    )
  }

  return (
    <RecoverComponent
      visible="true"
      dc={ dc }
      testMode={ testMode }
      ethAddress={ ethAddress }
      recoverContainer={ RecoverContainer }
    />
  )
}

export default Recover;
