import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import assert from 'assert';
import qs from 'qs';
import PageContainer from '../components/PageContainer';
import Connect from '../services/connectWeb3'
import { QueryString } from '../types/types'
import { TextInputHeader, TextBox, icons, text } from 'dark-crystal-web3-ui-components';
import { Owner } from '../types/types';
import {
  PageHeaderStoreConnecting,
  PageHeaderStoreSending,
  PageHeaderStoreSuccess,
  PageHeaderStoreError,
  MetamaskErrorCodes,
  BackupAlreadyPublishedError,
  WillOverwriteExistingBackupError,
} from '../services/copy'

// TODO this should display not the eth address, but whatever the chosen lookup key was.
// Which is tricky to do without some sort of global state between pages

const Store = () => {
  // stateMessage is used to track errors as well as the states 'sending' and 'success'
  const [stateMessage, setStateMessage] = useState<string>('connecting')
  const [owner, setOwner] = useState<Owner>({})
  const [didAttemptPublish, setDidAttemptPublish] = useState<boolean>(false)

  const router = useRouter()

  const fetchOwner = async (): Promise<Owner> => {
    const result = await Connect();
    setOwner(result)
    return result
  }

  useEffect(() => {
    fetchOwner()
  }, [])

  // Detect change in Metamask account
  useEffect(() => {
    if (window.ethereum) {
      window.ethereum.on("chainChanged", () => {
        fetchOwner();
      });
      window.ethereum.on("accountsChanged", () => {
        fetchOwner();
      });
    }
  });

  useEffect(() => {
    if (owner.contract && !didAttemptPublish) {
      attemptPublish()
    }
  })

  function attemptPublish () {
    const query: string = qs.stringify(router.query, { encode: false })
    const parsed: QueryString = qs.parse(query)
    console.log('parsed', parsed)

    try {
      assert(Object.keys(parsed).length, 'No parameters passed')
      assert(parsed.pk && parsed.c && parsed.s && parsed.lk, 'Incorrect parameters. Maybe you copied the link incorrectly')
      assert(parsed.lk.length === parsed.s.length, 'Mismatching number of shares and lookup-keys')

      parsed.pk = "0x".concat(parsed.pk)
      parsed.c = "0x".concat(parsed.c)
      for (let i = 0; i < parsed.lk.length; i++) {
        parsed.lk[i] = "0x".concat(parsed.lk[i])
          parsed.s[i] = "0x".concat(parsed.s[i])
      }
      const contract = owner.contract
      assert(contract && contract.methods, 'Cannot access contract methods')

      getPublishedStatus(parsed.lk, parsed.s, contract)
        .then(({ identicalPublished, willClobber }) => {
            if (identicalPublished) {
              setStateMessage(BackupAlreadyPublishedError)
            } else if (willClobber) {
              setStateMessage(WillOverwriteExistingBackupError)
            } else {
              assert(owner.accounts, 'Please connect your wallet and try again')
              setDidAttemptPublish(true)
              contract.methods.storeShares(parsed.pk, parsed.c, parsed.s, parsed.lk)
                .send({ from: owner.accounts[0] }, (err: any) => {
                  if (err) {
                    console.log('this error happened here: ', err)
                    setStateMessage(resolveErrorMessage(err))
                  } else {
                    setStateMessage('sending')
                  }
                })
                .on('receipt', (receipt: any) => {
                  console.log('Success! Your secret has been saved. Here is your transaction receipt: ', receipt)
                  setStateMessage('success')
                })
                .on('error', (err: Error, receipt: any) => {
                  console.log('error: ', err)
                  console.log('receipt: ', receipt)
                  setStateMessage(resolveErrorMessage(err))
                })
            }
        })
        .catch((err) => {
            setStateMessage(err.message)
        })
    } catch (err: any) {
      setStateMessage(err.message)
    }
  }

  async function getPublishedStatus (lookupKeys: Array<string>, shares: Array<string>, contract: any) {
    let willClobber = false
    let identicalPublished = true
    for (const index in lookupKeys) {
      try {
        const result = await contract.methods.getShare(lookupKeys[index]).call()
        if (result.share !== shares[index]) {
          identicalPublished = false
          willClobber = true
        }
      } catch (err) {
        identicalPublished = false
      }
    }
    return { identicalPublished, willClobber }
  }

  const displayStates = {
    'connecting': (
      <PageContainer heading={ PageHeaderStoreConnecting } bgColour='bg-secret-owner'>
      </PageContainer>
    ),
    'sending': (
      <PageContainer heading={ PageHeaderStoreSending } bgColour='bg-secret-owner'>
      </PageContainer>
    ),
    'success': (
      <PageContainer heading={ PageHeaderStoreSuccess } bgColour='bg-secret-owner'>
        <div>
          <TextInputHeader number="1" text={ text.successfullyPublishedAction } />
          <TextBox
            text={ owner.accounts ? owner.accounts[0] : 'Cannot retieve Eth address' }
            icon={ icons('eth-logo') }
          />
        </div>
      </PageContainer>
    ),
    // TODO add some suggestions to fix the problem, eg: was the link copied wrong?
    'unsuccessful': (
      <PageContainer heading={ PageHeaderStoreError } bgColour='bg-secret-owner'>
        <div>
          { stateMessage.split('\n').map((line) => {
            return (
              <p className="p-2" key={ line }>
                <strong className="text-pink-600 text-3xl">{ line }</strong>
              </p>
            )
          }) }
        </div>
      </PageContainer>
    ),
  }

  return displayStates[stateMessage] || displayStates['unsuccessful']
}

function resolveErrorMessage(err: any) {
  return err.code && Object.keys(MetamaskErrorCodes).includes(err.code.toString())
    ? MetamaskErrorCodes[err.code]
    : err.message
}

export default Store;
