import getWeb3 from "./getWeb3";
import SecretsContract from 'dark-crystal-web3-contracts/build/contracts/Secrets.json';
import Web3 from "web3";
import { Contract } from "web3-eth-contract"
import { Owner } from '../types/types'

const connect = async (): Promise<Owner> => {
  try {
    const web3: Web3 = await getWeb3()
    // Load account
    const accounts : string[] = await web3.eth.getAccounts()
    const networkId : number = await web3.eth.net.getId()
    const networkData : any = SecretsContract.networks
    const network : any = networkData[networkId]

    console.log(`{
      'account: ', ${accounts[0]},
      'contract: ', ${network.address},
    }`)

    if (network) {
      const Secrets : Contract = new web3.eth.Contract(
        SecretsContract.abi as any,
        network && network.address,
      );
      return { web3, accounts, contract: Secrets }
    } else {
      window.alert(`
        Secrets contract not deployed to detected network
        
        Please check that your wallet is connected to Goerli
      `)
      return {}
    }
  } catch (error) {
    // Catch any errors for any of the above operations.
    alert(`
      Failed to load find account or contract

      Please check that your wallet is connected to Goerli
    `);
    console.error(error);
    return {}
  }
};

export default connect
