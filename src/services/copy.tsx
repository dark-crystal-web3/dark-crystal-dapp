import {
  MainMenuHeader,
  MainMenuOption,
  MenuOption,
  PageHeaderText,
  PageContentText
} from '../types/types'

export const MainMenuOwner: MainMenuHeader = {
  menu: 'Secret Owner',
  title: 'I HAVE A SECRET',
  subtitle: 'You want to safely backup a secret using the distributed web',
  colour: 'bg-secret-owner',
  border: 'border-y-8'
}

export const MainMenuPartner: MainMenuHeader = {
  menu: 'Recovery Partner',
  title: 'I\'M A RECOVERY PARTNER',
  subtitle: 'Someone who trusts you has asked you to help them safely backup a secret',
  colour: 'bg-off-white',
  border: 'border-y-0'
}

export const MainMenuBackup: MainMenuOption = {
  icon: 'backupOpen',
  alt: 'An open treasure chest',
  title: 'BACK UP A SECRET',
  subtitle: 'To backup a secret you need to choose recovery partners and request their public keys',
  text: 'Your secret will be split into a set of backup pieces, each linked to one of these keys, and stored on the distributed web.',
  page: '/backup'
}

export const MainMenuRecover: MainMenuOption = {
  icon: 'crystalsRecovered',
  alt: 'A bouquet of crystals',
  title: 'RECOVER A SECRET',
  subtitle: 'To recover a secret you will need the unencrypted shares from your recovery partners',
  text: 'To recover a secret you need to give your reovery partners the "identifier" you used during backup (usually your ethereum address), and ask them to retrieve their backup piece and send it to you.',
  page: '/recover'
}

export const MainMenuGenerate: MainMenuOption = {
  icon: 'keyGenerate',
  alt: 'A plain gold key',
  title: 'GENERATE A PUBLIC KEY',
  subtitle: 'A secret owner has asked you to generate a public key',
  text: 'As a recovery partner, you need to send a public key to the secret-owner so that they can backup their secret.',
  page: '/generate'
}

export const MainMenuDecrypt: MainMenuOption = {
  icon: 'keyDecrypt',
  alt: 'A jeweled gold key',
  title: 'DECRYPT A PIECE',
  subtitle: 'If a contact has lost access to their secret, they will ask you to retrieve a backup piece and send it to them',
  text: 'This will allow you to retrieve and decrypt the backup piece. It is important to make sure you are sending the backup piece to the right person.',
  page: 'decrypt'
}

export const FAQ: MenuOption = {
  title: 'FAQs',
  page: 'https://darkcrystal.pw/web3'
}

export const About: MenuOption = {
  title: 'About Dark Crystal',
  page: 'https://darkcrystal.pw'
}

export const PageHeaderGenerate: PageHeaderText = {
  icon: 'keyGenerate',
  alt: 'A plain gold key',
  title: 'Generate A Public Key',
  subtitle: 'A secret-owner will need a public key from you in order to store their secret',
}

export const PageContentGenerate: PageContentText = {
  label: 'Generate a public key to send to a secret-owner',
  subtext: ['Make sure you are connected to an Ethereum wallet -> Read more'],
  placeholder: 'Connect to your Ethereum wallet',
}

export const PageHeaderGenerateSuccess: PageHeaderText = {
  icon: 'successCup',
  alt: 'A trophy cup bearing an engraving of a tick',
  title: 'Your public key is ready!',
  subtitle: 'A public key has successfully been created',
}

export const PageContentGenerateSuccess: PageContentText = {
  label: 'Your public key is ready!',
  subtext: ['Send this public key to the secret-owner', 'Always use a secure and encrypted channel for sending keys -> Read more'],
  placeholder: '',
}

export const PageHeaderDecrypt: PageHeaderText = {
  icon: 'keyDecrypt',
  alt: 'A jeweled gold key',
  title: 'Decrypt a piece',
  subtitle: 'A secret-owner needs your help to recover their secret',
}

export const PageContentDecryptOne: PageContentText = {
  label: 'Connect your wallet',
  subtext: ['Make sure you are connected to your Ethereum account -> Read more', 'Use the same Ethereum account that you used before'],
  placeholder: '',
}

export const PageContentDecryptTwo: PageContentText = {
  label: 'Enter the identifier',
  subtext: ['Enter the unique identifier given to you by your secret-owner - usually their Ethereum address'],
  placeholder: "Enter secret-owner's ethereum address or other identifier",
}

export const PageHeaderDecryptSuccess: PageHeaderText = {
  icon: 'successCup',
  alt: 'A trophy cup bearing an engraving of a tick',
  title: 'Your piece was decrypted!',
  subtitle: 'Your piece of the secret is ready',
}

export const PageContentDecryptError: PageContentText = {
  label: 'Error: No share was found',
  subtext: ['Check that you are using your correct ethereum account', 'Check that you are using the correct identifier given to you by the secret-owner'],
  placeholder: '',
}

export const PageContentDecryptSuccess: PageContentText = {
  label: 'Decrypted piece',
  subtext: ['Always use a secure and encrypted channel for sending keys'],
  placeholder: '',
}

export const PageHeaderStoreConnecting: PageHeaderText = {
  icon: 'keyGenerate', // TODO spinner?
  alt: 'A plain gold key',
  title: 'Connecting to web3',
  subtitle: 'You will be asked to sign a transaction',
}

export const PageHeaderStoreSending: PageHeaderText = {
  icon: 'keyDecrypt', // TODO spinner?
  alt: 'A jeweled gold key',
  title: 'Publishing transaction...',
  subtitle: 'Waiting for confirmation that the backup is successfully published. This may take a few minutes...',
}

export const PageHeaderStoreSuccess: PageHeaderText = {
  icon: 'successCup',
  alt: 'A trophy cup bearing an engraving of a tick',
  title: 'Your secret is safe now.',
  subtitle: 'Successfully published your shares on the distributed web.',
}

export const PageHeaderStoreError: PageHeaderText = {
  icon: 'skull',
  alt: 'Skull icon',
  title: 'Cannot publish backup!',
  subtitle: '',
}

export const PageHeader404: PageHeaderText = {
  icon: 'skull',
  alt: 'Skull icon',
  title: '404 - Route not found',
  subtitle: '',
}

export const MetamaskErrorCodes = {
  4001: 'You chose to cancel the transaction',
  4901: 'It looks like you are not connected to the right network. Make sure you are connected to Goerli.',
  4900: 'It looks like you are not connected to the right network. Make sure you are connected to Goerli.',
  32003: 'The transaction was rejected. Check that you have sufficient funds and that your account is not locked.',
}

export const BackupAlreadyPublishedError = 'This backup has already been published. Your secret is backed up.'

export const WillOverwriteExistingBackupError = 'This backup would overwrite an existing one as the identifier has already been used with one or more of these contacts.\nTo be able to publish this backup, you need to choose either a different identifier, or different contacts.'

export const AppName: string = 'DARK CRYSTAL'

export const AppTag: string = 'Social Recovery Dapp'

export const Footer: string = 'darkcrystal.pw | magmacollective.org'
