import Connect from './connectWeb3'
import { Owner, Keypair } from '../types/types'
import Sodium from 'sodium-javascript';

const createKeyPair = async (owner: Owner) : Promise<Keypair> => {

  try {
    if (owner.web3 && owner.accounts) {
      const { web3, accounts } = owner;
      const string = "Dark Crystal Secret Share"
      // check do we need to hash string into a 32byte message:
      // const sha3string = await web3.utils.sha3(string)
      const result = await web3.eth.personal.sign(string, accounts[0], '')
        .then(signedString => {
          return createKeys(signedString)
        })
      return result
    } else {
      const result = await Connect();
      owner = result
      return createKeyPair(owner)
    }
  } catch (error) {
    console.error(error)
    throw error
  }

  function createKeys(signedString: string) {
    let keyPair: Keypair = {
      newPublicKey: Buffer.alloc(Sodium.crypto_box_PUBLICKEYBYTES),
      newSecretKey: Buffer.alloc(Sodium.crypto_box_SECRETKEYBYTES)
    }
    const seed = Buffer.alloc(Sodium.crypto_box_SEEDBYTES, signedString)
    Sodium.crypto_box_seed_keypair(keyPair.newPublicKey, keyPair.newSecretKey, seed)
    return keyPair
  };
};

export default createKeyPair