import * as dcShare from 'web3-backup-js-binding'

function share(publicKeysHex: string[], secret: string, lookupKey: string) {
  const output = JSON.parse(
    dcShare.share(
      JSON.stringify({
        public_keys: publicKeysHex,
        secret,
        lookup_key: lookupKey,
      })
    )
  )
  if (output.Error) throw new Error(output.Error)
  return output.Success
}

function recover(shares: string[]) {
  const output = JSON.parse(dcShare.recover(JSON.stringify({ shares })))
  if (output.Error) throw new Error(output.Error)
  return output.Success
}

export { share, recover }
