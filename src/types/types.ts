import { Contract } from "web3-eth-contract"
import Web3 from 'web3';

export type Owner = {
  accounts? : string[];
  web3? : Web3;
  contract?: Contract;
}

export type Keypair = {
  newPublicKey?: Buffer,
  newSecretKey?: Buffer
}

export type ActionOption = {
  option: string;
}

export type LinkValue = {
  value: string;
}

export type MainMenuHeader = {
  menu: string
  title: string, 
  subtitle: string,
  colour: string,
  border: string
}

export type MainMenuOption =
  PageHeaderText & {
  text: string,
  page: string
}

export type MenuOption = {
  title: string,
  page: string
}

export type PageHeaderText = {
  icon: string,
  alt: string,
  title: string,
  subtitle: string,
  subsubtitle?: string,
}

export type PageContentText = {
  label: string,
  subtext: string[],
  placeholder: string
}

export type Key = string

export type QueryString = {
  pk?: string,
  c?: string,
  s?: [string],
  lk?: [string]
}
