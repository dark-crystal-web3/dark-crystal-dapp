/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/images/**/*.{js,ts,jsx,tsx,png}",
    "./node_modules/dark-crystal-web3-ui-components/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'hero-pattern': "url('/img/backgroundPageHeader.png')",
      },
      boxShadow: {
        '8px': '-8px 8px 0px',
        '6px': '-6px 6px 0px'
      },
      colors: {
        'white': '#FFFFFF',
        'off-white': '#ECDFDD',
        'very-light-grey': '#E5E5E5',
        'very-dark-pink': '#4F1C35',
        'strong-pink': '#B30E5A',
        'barpink': '#821A4E',
        'grey-pink': '#821a4e66',
        'barpink-end': '#821a4e00',
        'secret-owner': '#D7B8C0',
        'very-pale-pink': '#D7B8C0',
        'pale-pink': '#CAA0AF',
        'dark-pink': '#821A4E',
      },
      fontFamily: {
        'silkscreen': ['Silkscreen'],
        'inter': ['Inter']
      },
      fontSize: {
        'navbarDC': ['30px', {
          lineheight: '90%',
          letterSpacing: '-0.05em'
        }]
      },
      lineHeight: {
        'over': '150%'
      },
      spacing: {
        'box': '0.625rem'
      },
      screens: {
        'xs': '480px',
        'sm': '640px',
        'md': '768px',
        'lg': '1024px',
        'xl': '1280px', // Original design size
        'xxl': '1450px',
        '2xl': '1536px'
      }
    },
  },
  plugins: [],
}
